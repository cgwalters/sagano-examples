#!/bin/bash
# This is basically a stock virt-install invocation to boot a VM
# and auto-install with a kickstart.  But an important point
# is that this also works on bare metal the same as every
# other Anaconda installation.
# https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/configuring_and_managing_virtualization/assembly_creating-virtual-machines_configuring-and-managing-virtualization
set -xeuo pipefail
exec virt-install --connect qemu:///system --name sagano-demo --memory 2048 --vcpus 4 --disk size=40 \
          --os-variant rhel9.0 --location https://dl.fedoraproject.org/pub/fedora/linux/releases/38/Everything/x86_64/os/ \
          --noautoconsole --initrd-inject $(pwd)/basic.ks --extra-args="inst.ks=file:/basic.ks console=ttyS0,115200n8"
