text

# Basic partitioning
clearpart --all --initlabel --disklabel=gpt
part prepboot  --size=4    --fstype=prepboot
part biosboot  --size=1    --fstype=biosboot
part /boot/efi --size=100  --fstype=efi
part /boot     --size=1000  --fstype=ext4 --label=boot
part / --grow --fstype xfs

ostreecontainer --url quay.io/cgwalters/ostest	--no-signature-verification

firewall --disabled
services --enabled=sshd

# Only inject a SSH key for root
rootpw --iscrypted locked
sshkey --username root "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOQkQHeKan3X+g1jILw4a3KtcfEIED0kByKGWookU7ev demo@example.com"
reboot

# Install via bootupd - TODO change anaconda to auto-detect this
bootloader --location=none --disabled
%post --erroronfail
# Work around anaconda wanting a root password
passwd -l root
/usr/bin/bootupctl backend install -vvvv --auto --with-static-configs --device /dev/vda /
%end
